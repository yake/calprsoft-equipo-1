﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.CapaDominio.Contratos
{
    public interface IComponenteDao
    {
        Boolean actualizarComponte(Venta venta);

        List<Componente> buscarComponentes(string componente);
    }
}
