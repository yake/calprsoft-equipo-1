﻿using GestionReparacion.CapaDominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionReparacion.CapaDominio.Contratos
{
    public interface IPedidoComponenteDao
    {
        Boolean guardarPedidoComponente(Venta venta);
    }
}
