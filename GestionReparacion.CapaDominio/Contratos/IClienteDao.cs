﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.CapaDominio.Contratos
{
    public interface IClienteDao
    {
        void guardarCliente(Cliente cliente);

        void actualizarCliente(Cliente cliente);

        Cliente buscarClientePorDni(string dni);

    }
}
