﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.CapaDominio.Contratos
{
    public interface IEmpleadoDao
    {
        void guardarEmpleado(Empleado empleado);

        Boolean actualizarEmpleado(Venta venta);

        Empleado obtenerEmpleado();

        Empleado loguearEmpleado(string dni);
    }
}
