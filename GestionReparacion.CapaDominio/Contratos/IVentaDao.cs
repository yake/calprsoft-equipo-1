﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.CapaDominio.Contratos
{
    public interface IVentaDao
    {
        Boolean guardarVenta(Venta venta, Cliente cliente);
        Venta obtenerVenta();
        List<Venta> obtenerListaDeVentas(string dni);
        Venta obtenerListaDePedidosDeUnaVenta(Venta venta);
        void actualizarVenta(int id_venta);
        Venta obtenerVentaPorId(int id_venta);

    }
}
