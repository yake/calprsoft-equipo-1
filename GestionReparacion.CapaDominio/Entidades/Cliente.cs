﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionReparacion.CapaDominio.Entidades
{
    public class Cliente : Persona
    {
        private String direccionDelivery;

        public string DireccionDelivery { get => direccionDelivery; set => direccionDelivery = value; }

    }
}
