﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionReparacion.CapaDominio.Entidades
{
    public class TarjetaDePago
    {
        private string numeroDeTarjeta;
        private string codigoCSC;

        public string NumeroDeTarjeta { get => numeroDeTarjeta; set => numeroDeTarjeta = value; }
        public string CodigoCSC { get => codigoCSC; set => codigoCSC = value; }

        public bool esValidoNumeroDeTarjeta()
        {
            return numeroDeTarjeta.Length == 16;
        }
        public bool esValidoCodigoCSC()
        {
            return codigoCSC.Length == 3;
        }
    }
}
