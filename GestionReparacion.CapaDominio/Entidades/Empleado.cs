﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionReparacion.CapaDominio.Entidades
{
    public class Empleado : Persona
    {
        private int tareasPendientes = 0;
        private double sueldo = 0;
        private int cantidadDePenalidades = 0;

        public int CantidadDeEnsamblajesAsignados { get => tareasPendientes; set => tareasPendientes = value; }
        public double Sueldo { get => sueldo; set => sueldo = value; }
        public int CantidadDePenalidades { get => cantidadDePenalidades; set => cantidadDePenalidades = value; }


        public bool estaPenalizado()
        {
            return cantidadDePenalidades >= 3;
        }


    }
}
