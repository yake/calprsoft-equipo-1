﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionReparacion.CapaDominio.Entidades
{
    public class Venta
    {

        private int id;
        private bool esServicioDeEnsamblajeAceptado;
        private bool esServicioDeEnsamblajeTerminado;

        private Empleado empleado = new Empleado();
        private List<PedidoComponente> listaPedidoComponentes = new List<PedidoComponente>();
        private DateTime fecha = new DateTime();

        private double costoTotal;

        public Venta()
        {
            this.Fecha = DateTime.Today;
            this.esServicioDeEnsamblajeTerminado = false;
        }

        public int Id { get => id; set => id = value; }
        public bool EsServicioDeEnsamblajeAceptado { get => esServicioDeEnsamblajeAceptado; set => esServicioDeEnsamblajeAceptado = value; }
        public Empleado Empleado { get => empleado; set => empleado = value; }
        public List<PedidoComponente> ListaPedidoComponentes { get => listaPedidoComponentes; set => listaPedidoComponentes = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
        public double CostoTotal { get => costoTotal; set => costoTotal = value; }
        public bool EsServicioDeEnsamblajeTerminado { get => esServicioDeEnsamblajeTerminado; set => esServicioDeEnsamblajeTerminado = value; }

        public double calcularMontoPedido()
        {
            double montoTotal = 0.0;
            double costoPorDelivery = 8;

            foreach (var item in listaPedidoComponentes)
            {
                montoTotal += item.calcularCostoTotal();
            }

            return montoTotal + costoPorDelivery;
        }

        public double calcularCostoDeEnsamblado()
        {
            double costoEnsamblado = 0.0;

            if (esServicioDeEnsamblajeAceptado)
            {
                foreach (var item in listaPedidoComponentes)
                {
                    costoEnsamblado += (item.Componente.Dificultad / 100.0) * item.calcularMontoTotal();
                }
            }

            return costoEnsamblado;
        }

        public double calcularCostoTotal()
        {
            return calcularCostoDeEnsamblado() + calcularMontoPedido();
        }

        public bool tieneServicioDeEnsamblaje()
        {
            return esServicioDeEnsamblajeAceptado;
        }

        public bool estaElPlazoVencido()
        {
            DateTime fechaVencimiento = Fecha.AddDays(2);
            DateTime fechaActual = DateTime.Today;

            int resultadoCompararFechas = DateTime.Compare(fechaActual, fechaVencimiento);

            if (resultadoCompararFechas > 0) // fechaActual > fechaVencimiento
            {
                return true;
            }
            return false;
        }

        public void aumentarPenalizacionEmpleado()
        {
            if (estaElPlazoVencido() && !empleado.estaPenalizado())
            {
                empleado.CantidadDePenalidades++;
            }
        }

        public void asignarEmpleado()
        {
            empleado.CantidadDeEnsamblajesAsignados++;
        }

        public void actualizarStockComponentes()
        {
            foreach (var item in ListaPedidoComponentes)
            {
                item.actualizarStock();
            }
        }

    }
}
