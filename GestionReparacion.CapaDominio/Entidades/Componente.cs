﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionReparacion.CapaDominio.Entidades
{
    public class Componente
    {

        private int idComponente;
        private String nombreComponente;
        private String tipo;
        private String marca;
        private double precio;
        private Int32 stock;
        private int dificultad;
        private bool check = false;
        private String categoria;

        public int IdComponente { get => idComponente; set => idComponente = value; }
        public string NombreComponente { get => nombreComponente; set => nombreComponente = value; }
        public string Tipo { get => tipo; set => tipo = value; }
        public string Marca { get => marca; set => marca = value; }
        public double Precio { get => precio; set => precio = value; }
        public Int32 Stock { get => stock; set => stock = value; }
        public int Dificultad { get => dificultad; set => dificultad = value; }
        public bool Check { get => check; set => check = value; }
        public string Categoria { get => categoria; set => categoria = value; }

        public bool estaDisponible()
        {
            return stock > 0;
        }
        
    }
}
