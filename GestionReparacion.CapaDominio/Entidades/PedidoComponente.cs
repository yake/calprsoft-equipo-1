﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionReparacion.CapaDominio.Entidades
{
    public class PedidoComponente
    {
        private int idPedidoComponente;
        private int cantidadComponentes;
        private Componente componente;

        public PedidoComponente()
        {
            componente = new Componente();
        }

        public int IdPedidoComponente { get => idPedidoComponente; set => idPedidoComponente = value; }
        public int CantidadComponentes { get => cantidadComponentes; set => cantidadComponentes = value; }
        //public double CostoTotalComponentes { get => costoTotalComponentes; set => costoTotalComponentes = value; }
        public Componente Componente { get => componente; set => componente = value; }

        // Calculo descuento por mes festivo
        public double calcularDescuentoPorMesFestivo()
        {
            double descuento = 0.0;
            DateTime fechaActual = DateTime.Today;
            string mesActual = fechaActual.ToString("MMMM").ToLower();

            if(mesActual == "febrero" || mesActual == "julio" || mesActual == "diciembre")
            {
                descuento = 0.05 * calcularMontoTotal();
            }

            return descuento;
        }

        // Calculo descuento por cantidades mayores a 10 componentes
        public double calcularDescuentoPorCantidad()
        {
            double descuento = 0.0;

            if (cantidadComponentes > 10)
            {
                descuento = 0.1 * calcularMontoTotal();
            }

            return descuento;
        }

        //Se calcula el descuento total por componente
        public double calcularDescuentoTotal()
        {
            return calcularDescuentoPorCantidad() + calcularDescuentoPorMesFestivo();
        }

        //Se calcula el costo todal del compoente aplicando descuento
        public double calcularCostoTotal()
        {
            return calcularMontoTotal() - calcularDescuentoTotal();
        }

        //Se calcula el monto total por componente
        public double calcularMontoTotal()
        {
            return componente.Precio * cantidadComponentes;
        }

        //Valida si la cantidad ingresada es valida
        public Boolean esValidoCantidad()
        {
            return cantidadComponentes <= componente.Stock;
        }

        //Actualiza el stock
        public Boolean actualizarStock()
        {
            if (esValidoCantidad())
            {
                componente.Stock -= cantidadComponentes;

                return true;
            }

            return false;
        }
    }
}
