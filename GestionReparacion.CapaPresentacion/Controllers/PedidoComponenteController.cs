﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionReparacion.CapaDominio.Entidades;
using GestionReparacion.CapaAplicacion.Servicios;

namespace GestionReparacion.CapaPresentacion.Controllers
{
    public class PedidoComponenteController : Controller
    {
        List<Componente> componentes = new List<Componente>();
        List<PedidoComponente> pedidoComponentes = new List<PedidoComponente>();
        Cliente cliente = new Cliente();
        Venta venta = new Venta();

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string dni)
        {
            var gestionarReparacionDispositivoServicio = new GestionarReparacionDispositivoService();
            cliente = gestionarReparacionDispositivoServicio.obtenerCliente(dni);

            if (cliente != null)
            {
                Session["Cliente"] = cliente;
                return RedirectToAction("BuscarComponentes");
            }
            else
            {
                return View();
            }

        }

        [HttpGet]
        public ActionResult BuscarComponentes()
        {
            if (Session["Cliente"] == null)
            {
                return RedirectToAction("Login");
            }
            else
            {
                Session["pedidoComponentes"] = pedidoComponentes;
                return View((List<Componente>)Session["ListaComponentes"]);
            }
        }

        [HttpPost]
        public ActionResult BuscarComponentes(string categoria)
        {
            var gestionarReparacionDispositivos = new GestionarReparacionDispositivoService();

            componentes = gestionarReparacionDispositivos.buscarComponentes(categoria);

            Session["ListaComponentes"] = componentes;

            return View(componentes);
        }

        [HttpGet]
        public ActionResult AgregarComponentes(int cantidad, int componenteId)
        {
            PedidoComponente pedido = new PedidoComponente();
            componentes = (List<Componente>)Session["ListaComponentes"];
            pedidoComponentes = (List<PedidoComponente>)Session["pedidoComponentes"];

            Session["Venta"] = venta;

            try
            {

                foreach (var item in componentes)
                {
                    if (item.IdComponente == componenteId) pedido.Componente = item;
                }

                pedido.CantidadComponentes = cantidad;

                var gestionarReparacionDispositivos = new GestionarReparacionDispositivoService();

                gestionarReparacionDispositivos.añadirPedido(pedido, ref pedidoComponentes);

            }
            catch (Exception e)
            {
                ((Venta)Session["Venta"]).ListaPedidoComponentes = pedidoComponentes;

                return Json(new { respuesta = false, mensaje = e.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { respuesta = true}, JsonRequestBehavior.AllowGet );
        }

        [HttpGet]
        public ActionResult ListarPedido()
        {
            var gestionarReparacionDispositivos = new GestionarReparacionDispositivoService();

            venta = (Venta)Session["Venta"];
            venta.ListaPedidoComponentes = ((List<PedidoComponente>)Session["pedidoComponentes"]);

            ViewBag.CostoComponentes = gestionarReparacionDispositivos.
                obtenerCostoPedido(venta);

            return View(venta.ListaPedidoComponentes);
        }

        [HttpGet]
        public ActionResult AgregarEnsamblado(Boolean ensamblado)
        {
            var gestionarReparacionDispositivos = new GestionarReparacionDispositivoService();
            string mensaje = "";

            venta.ListaPedidoComponentes = ((List<PedidoComponente>)Session["pedidoComponentes"]);
            venta.EsServicioDeEnsamblajeAceptado = ensamblado;

            try
            {
                if (ensamblado)
                {
                    ViewBag.CostoComponentes = gestionarReparacionDispositivos.
                        agregarServicioEnsamblado(venta);

                    mensaje = "Se agrego el servicio de ensamblado";
                }

                else
                {
                    ViewBag.CostoComponentes = gestionarReparacionDispositivos.
                        obtenerCostoPedido(venta);

                    mensaje = "Se quito el servicio de ensamblado";
                }
            }
            catch (Exception e)
            {
                return Json(new { respuesta = false, e.Message }, JsonRequestBehavior.AllowGet);
            }

            Session["Venta"] = venta;

            return Json(new { respuesta = true, mensaje, costo = ViewBag.CostoComponentes }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult PagarComponente()
        {
            venta = (Venta)Session["Venta"];

            var gestionarReparacionDispositivos = new GestionarReparacionDispositivoService();

            ViewBag.CostoComponentes = gestionarReparacionDispositivos.
                obtenerCostoPedido(venta);

            ViewBag.CostoEnsamblaje = gestionarReparacionDispositivos.
                obtenerCostoEnsamblado(venta);

            ViewBag.CostoTotal = gestionarReparacionDispositivos.
                obtenerCostoTotal(venta);

            return View();
        }

        [HttpPost]
        public ActionResult GuardarPedido(string numTarjeta, string numCsc)
        {
            venta = (Venta)Session["Venta"];
            
            var gestionarReparacionDispositivos = new GestionarReparacionDispositivoService();

            TarjetaDePago tarjetaDePago = new TarjetaDePago();

            tarjetaDePago.CodigoCSC = numCsc;
            tarjetaDePago.NumeroDeTarjeta = numTarjeta;

            gestionarReparacionDispositivos.guardarPedido(tarjetaDePago, venta, (Cliente)Session["Cliente"]);

            return RedirectToAction("MostrarInformeDetallado");
        }

        [HttpGet]
        public ActionResult MostrarInformeDetallado()
        {
            venta = (Venta)Session["Venta"];

            var gestionarReparacionDispositivos = new GestionarReparacionDispositivoService();

            ViewBag.CostoComponentes = gestionarReparacionDispositivos.
                obtenerCostoPedido(venta);

            ViewBag.CostoEnsamblaje = gestionarReparacionDispositivos.
                obtenerCostoEnsamblado(venta);

            ViewBag.CostoTotal = gestionarReparacionDispositivos.
                obtenerCostoTotal(venta);

            return View(venta);
        }
    }
}
