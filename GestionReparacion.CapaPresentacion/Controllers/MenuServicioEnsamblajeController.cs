﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionReparacion.CapaDominio.Entidades;
using GestionReparacion.CapaAplicacion.Servicios;

namespace GestionReparacion.CapaPresentacion.Controllers
{
    public class MenuServicioEnsamblajeController : Controller
    {
        Empleado empleado = null;

        [HttpGet]
        public ActionResult Login()
        {
            return View(Session["Mensaje"]);
        }


        [HttpPost]
        public ActionResult Login(string dni)
        {
            var asignarEnsamblajeServico = new AsignarEnsamblajeServicio();
            empleado = asignarEnsamblajeServico.loguearEmpleado(dni);

            if(empleado != null)
            {
                Session["Empleado"] = empleado;
                return RedirectToAction("MostrarServiciosAsignados");
            }
            else
            {
                Session["Mensaje"] = "No existe ningun empleado con el DNI ingresado";
                return View(Session["Mensaje"]);
            }

        }

        [HttpGet]
        public ActionResult MostrarServiciosAsignados()
        {
            Empleado empleado = (Empleado)Session["Empleado"];

            if (empleado == null)
            {
                Session["Mensaje"] = "";
                return RedirectToAction("Login");
            }
            else if(empleado.estaPenalizado())
            {
                Console.WriteLine(empleado.CantidadDePenalidades);
                Session["Mensaje"] = "Usted está penalizado. Contacte con su supervisor";
                return RedirectToAction("Login");
            }
            else
            {
                Console.WriteLine(empleado.CantidadDePenalidades);
                var asignarEnsamblajeServico = new AsignarEnsamblajeServicio();
                List<Venta> ventasObtenidas = new List<Venta>();
                List<Venta> ventasCompletas = new List<Venta>();
                Empleado empleadoLogueado = (Empleado)Session["Empleado"];

                ventasObtenidas = asignarEnsamblajeServico.obtenerVentasPorDni(empleadoLogueado.Dni); //empleadoLogueado.Dni
                foreach (Venta item in ventasObtenidas)
                {
                    Venta venta = new Venta();
                    venta = asignarEnsamblajeServico.obtenerPedidosDeVenta(item);
                    venta.Empleado = (Empleado)Session["Empleado"];
                    ventasCompletas.Add(venta);
                }
                Session["Mensaje"] = "";
                Session["Ventas"] = ventasCompletas;

                return View(ventasCompletas);
            }
        }

        [HttpPost]
        public ActionResult ActualizarServicioEnsamblajeVenta(int id_venta)
        {
            var asignarEnsamblajeServico = new AsignarEnsamblajeServicio();
            asignarEnsamblajeServico.actualizarEnsamblajeVenta(id_venta);

            Venta venta = new Venta();
            venta.Empleado = (Empleado)Session["Empleado"];
            
            asignarEnsamblajeServico.actualizarEmpleado(venta);

            return RedirectToAction("MostrarServiciosAsignados");
        }

    }
}