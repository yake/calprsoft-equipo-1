﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionReparacion.CapaPresentacion.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Session["Venta"] = null;
            Session["ListaComponentes"] = null;
            Session["pedidoComponentes"] = null;

            return View();
        }
    }
}