﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionReparacion.CapaDominio.Contratos;
using GestionReparacion.CapaDominio.Entidades;
using GestionReparacion.CapaPersistencia.SQLServerDAO;

namespace GestionReparacion.CapaAplicacion.Servicios
{
    public class GestionarReparacionDispositivoService
    {
        private IClienteDao clienteDao;
        private IComponenteDao componenteDao;
        private IEmpleadoDao empleadoDao;
        private IGestorDAO gestorDAO;
        private IPersonaDao personaDao;
        private IVentaDao ventaDao;
        private IPedidoComponenteDao pedidoComponenteDao;
        private ITarjetaDePagoDao tarjetaDePagoDao;


        public GestionarReparacionDispositivoService()
        {
            gestorDAO = new GestorDAO();
            clienteDao = new ClienteDao(gestorDAO);
            componenteDao = new ComponenteDao(gestorDAO);
            empleadoDao = new EmpleadoDao(gestorDAO);
            personaDao = new PersonaDao(gestorDAO);
            ventaDao = new VentaDao(gestorDAO);
            pedidoComponenteDao = new PedidoComponenteDao(gestorDAO);
        }

        #region Antiguos

        /////////////// CAJA COMPONENTES /////////////////




        /////////////// EMEPLADO /////////////////
        
        public Boolean esCambiableEstado (Empleado empleado)
        {
            //return empleado.esCambiableEstado();
            return false;
        }

        public Boolean esEdadAceptable(Empleado empleado)
        {
            //return empleado.esEdadAceptable();
            return false;
        }

        public Boolean esLibre(Empleado empleado)
        {
            //return empleado.esLibre();
            return false;
        }

        /////////////// REPARACION /////////////////

        public Boolean validarReparacion (Venta reparacion)
        {
            /*if(!reparacion.esFechaEntregaValida())
            {
                throw new Exception("Fecha de entrega no valida");
            }

            if (!reparacion.esValidoCostoTotal())
            {
                throw new Exception("El costo total no es valido");
            }*/

            return true;
        }

        /*public void cambiarEstadoEmpleado(Reparacion reparacion)
        {
            gestorDAO.abrirConexion();
            reparacion.cambiarEstadoEmpleado();
            reparacionDao.actualizarReparacion(reparacion);
            gestorDAO.cerrarConexion();
        }
        */

        #endregion Antiguos

        ////////////////////////////////////////////////
        ////////////////////////////////////////////////
        ////////////////////////////////////////////////

        #region Nuevos

        /////////////// COMPONENTES ////////////////////

        public Cliente obtenerCliente(string dni)
        {
            Cliente cliente = new Cliente();

            gestorDAO.abrirConexion();
            cliente = clienteDao.buscarClientePorDni(dni);
            gestorDAO.cerrarConexion();

            return cliente;
        }

        public List<Componente> buscarComponentes(string categoria)
        {
            List<Componente> componetes = new List<Componente>(); 

            gestorDAO.abrirConexion();
            componetes = componenteDao.buscarComponentes(categoria);
            gestorDAO.cerrarConexion();

            return componetes;    
        }

        public void añadirPedido(PedidoComponente pedido, ref List<PedidoComponente> pedidoComponentes)
        {

            if (pedido.esValidoCantidad())
            {
                pedidoComponentes.Add(pedido);
                throw new Exception("Agregado Exitosamente");
            }

            else if (!pedido.Componente.estaDisponible())
            {
                throw new Exception("¡UPS!, No tenemos stock disponible para este componente.");
            }

            else
                throw new Exception("¡UPS!, La cantidad ingresada no es válida. ");
        }

        public double obtenerCostoPedido(Venta venta)
        {

            return (venta.calcularMontoPedido());
        }        
        
        public double agregarServicioEnsamblado(Venta venta)
        {
            return (venta.calcularCostoTotal());
        }

        public double obtenerCostoEnsamblado(Venta venta)
        {
            return (venta.calcularCostoDeEnsamblado());
        }

        public double obtenerCostoTotal(Venta venta)
        {

            return (venta.calcularCostoTotal());
        }

        public Boolean guardarPedido(TarjetaDePago tarjetaDePago, Venta venta, Cliente cliente)
        {
            Boolean guardar = false;

            if (!tarjetaDePago.esValidoNumeroDeTarjeta())
                throw new Exception("Numero de Tarjeta No Valido");

            else if (!tarjetaDePago.esValidoCodigoCSC())
                throw new Exception("Codigo Secreto de Tarjeta de Credito No Valido");

            Empleado empleado = obtenerEmpleado();

            venta.Empleado = empleado;

            venta.asignarEmpleado();

            venta.actualizarStockComponentes();

            guardar = guardarPedido(venta, tarjetaDePago, cliente);
            

            return guardar;
        }

        private Empleado obtenerEmpleado()
        {
            Empleado empleado = new Empleado();

            gestorDAO.abrirConexion();
            empleado = empleadoDao.obtenerEmpleado();
            gestorDAO.cerrarConexion();

            return empleado;

        }

        private Boolean guardarPedido (Venta venta, TarjetaDePago tarjetaDePago, Cliente cliente)
        {
            Boolean guardar = false;
            
            gestorDAO.abrirConexion();
                guardar = ventaDao.guardarVenta(venta, cliente);
            gestorDAO.cerrarConexion();

            gestorDAO.abrirConexion();
                
                Venta ventaGenerada = new Venta();
                ventaGenerada = ventaDao.obtenerVenta();
                ventaGenerada.ListaPedidoComponentes = venta.ListaPedidoComponentes;
                ventaGenerada.Empleado = venta.Empleado;
                venta = ventaGenerada;

            gestorDAO.cerrarConexion();

            /*gestorDAO.abrirConexion();
                guardar = ventaDao.guardarListaPedido(venta,venta1);
            gestorDAO.cerrarConexion();*/

            gestorDAO.abrirConexion();
                guardar = pedidoComponenteDao.guardarPedidoComponente(venta);
            gestorDAO.cerrarConexion();

            gestorDAO.abrirConexion();
                guardar = componenteDao.actualizarComponte(venta);
            gestorDAO.cerrarConexion();

            gestorDAO.abrirConexion();
                guardar = empleadoDao.actualizarEmpleado(venta);
            gestorDAO.cerrarConexion();

            return guardar;
        }

        #endregion Nuevos
    }
}
