﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionReparacion.CapaDominio.Contratos;
using GestionReparacion.CapaDominio.Entidades;
using GestionReparacion.CapaPersistencia.SQLServerDAO;

namespace GestionReparacion.CapaAplicacion.Servicios
{
    public class AsignarEnsamblajeServicio
    {
        private IEmpleadoDao empleadoDao;
        private IGestorDAO gestorDAO;
        private IVentaDao ventaDao;

        public AsignarEnsamblajeServicio()
        {
            gestorDAO = new GestorDAO();
            empleadoDao = new EmpleadoDao(gestorDAO);
            ventaDao = new VentaDao(gestorDAO);
        }

        public Empleado loguearEmpleado(string dni)
        {
            Empleado empleado = new Empleado();

            gestorDAO.abrirConexion();
            empleado = empleadoDao.loguearEmpleado(dni);
            gestorDAO.cerrarConexion();

            return empleado;

        }

        public Venta obtenerVentaPorId(int id_venta)
        {
            Venta venta = new Venta();

            gestorDAO.abrirConexion();
            venta = ventaDao.obtenerVentaPorId(id_venta);
            gestorDAO.cerrarConexion();

            return venta;
        }

        public List<Venta> obtenerVentasPorDni(string dni)
        {
            List<Venta> ventas = new List<Venta>();

            gestorDAO.abrirConexion();
            ventas = ventaDao.obtenerListaDeVentas(dni);
            gestorDAO.cerrarConexion();

            return ventas;
        }

        public Venta obtenerPedidosDeVenta(Venta venta)
        {
            gestorDAO.abrirConexion();
            venta = ventaDao.obtenerListaDePedidosDeUnaVenta(venta);
            gestorDAO.cerrarConexion();

            return venta;
        }

        public void actualizarEnsamblajeVenta(int id_venta)
        {
            gestorDAO.abrirConexion();
            ventaDao.actualizarVenta(id_venta);
            gestorDAO.cerrarConexion();
        }

        public Boolean actualizarEmpleado(Venta venta)
        {
            Boolean fueActualizado = false;

            venta.Empleado.CantidadDeEnsamblajesAsignados--;
            
            venta.Empleado.CantidadDePenalidades++;

            gestorDAO.abrirConexion();
            fueActualizado = empleadoDao.actualizarEmpleado(venta);
            gestorDAO.cerrarConexion();

            return fueActualizado;
        }
    }
}
