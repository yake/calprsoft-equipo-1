﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using GestionReparacion.CapaDominio.Contratos;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.CapaPersistencia.SQLServerDAO
{
    public class PedidoComponenteDao : IPedidoComponenteDao
    {
        private GestorDAO gestorDAO;

        public PedidoComponenteDao(IGestorDAO gestorDAO)
        {
            this.gestorDAO = (GestorDAO)gestorDAO;
        }

        public Boolean guardarPedidoComponente(Venta venta)
        {
            Boolean guardar = false;

            SqlCommand comando = null;

            string insertarPedidoComponenteSql;

            insertarPedidoComponenteSql = "INSERT INTO pedidoComponente (cantidad, componente_id, venta_id) " +
                                          "VALUES(@cantidad, @componente_Id_Fk, @venta_id_Fk)";
            
            try
            {

               
                ///// LISTA POR PEDIDO DE COMPONENTES ///////
                foreach (var item in venta.ListaPedidoComponentes)
                {
                    comando = gestorDAO.obtenerComandoSQL(insertarPedidoComponenteSql);

                    comando.Parameters.AddWithValue("@cantidad", item.CantidadComponentes);
                    comando.Parameters.AddWithValue("@componente_Id_Fk", item.Componente.IdComponente);
                    comando.Parameters.AddWithValue("@venta_id_Fk", venta.Id);

                    comando.ExecuteNonQuery();

                }
  

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar guardar los componentes ", err);
            }

            return guardar;

        }

    }
}
