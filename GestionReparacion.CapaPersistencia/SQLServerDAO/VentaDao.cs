﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionReparacion.CapaDominio.Contratos;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.CapaPersistencia.SQLServerDAO
{
    public class VentaDao : IVentaDao
    {
        private GestorDAO gestorDAO;

        public VentaDao(IGestorDAO gestorDAO)
        {
            this.gestorDAO = (GestorDAO)gestorDAO;
        }

        ///////////////////////////  VENTA ////////////////////////////////////////////

        public Venta obtenerVentaPorId(int id_venta)
        {
            Venta venta = new Venta();

            string obtenerVentaSql = "SELECT id, esServicioDeEnsamblajeTerminado, costoTotal, " +
                                     "fechaPago, empleado_id FROM dbo.venta where id = " + id_venta + ";";

            try
            {
                SqlDataReader resultadoSql = gestorDAO.ejecutarConsulta(obtenerVentaSql);

                if (resultadoSql.Read())
                {

                    venta.Id = resultadoSql.GetInt32(0);
                    venta.EsServicioDeEnsamblajeTerminado = resultadoSql.GetBoolean(1);
                    venta.CostoTotal = resultadoSql.GetDouble(2);
                    venta.Fecha = resultadoSql.GetDateTime(3);
                    venta.Empleado.IdPersona = resultadoSql.GetInt32(4);
                }

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar obtenete la venta", err);
            }

            return venta;
        }

        public Boolean guardarVenta(Venta venta, Cliente cliente)
        {
            string insertarVentaSQL;

            insertarVentaSQL = "INSERT INTO venta (esServicioDeEnsamblajeAceptado, esServicioDeEnsamblajeTerminado, costoTotal, fechaPago, empleado_id, cliente_id) " +
                               "VALUES (@esServicioDeEnsamblajeAceptado, @esServicioDeEnsamblajeTerminado, @costoTotal, @fechaPago, @empleado_Id_Fk, @cliente_id_Fk) ";

            try
            {
                SqlCommand comando;

                comando = gestorDAO.obtenerComandoSQL(insertarVentaSQL);

                //////// VENTA ////////////
                comando.Parameters.AddWithValue("@esServicioDeEnsamblajeAceptado", venta.EsServicioDeEnsamblajeAceptado);
                comando.Parameters.AddWithValue("@esServicioDeEnsamblajeTerminado", venta.EsServicioDeEnsamblajeTerminado);
                comando.Parameters.AddWithValue("@costoTotal", venta.calcularCostoTotal());
                comando.Parameters.AddWithValue("@fechaPago", venta.Fecha);
                comando.Parameters.AddWithValue("@empleado_Id_Fk", venta.Empleado.IdPersona);
                comando.Parameters.AddWithValue("@cliente_id_Fk", cliente.IdPersona);

                comando.ExecuteNonQuery();

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar guardar la venta", err);
            }

            return true;

        }


        public Venta obtenerVenta()
        {
            Venta venta = new Venta();

            string obtenerVentaSql = "SELECT TOP 1 id, esServicioDeEnsamblajeTerminado, costoTotal, " +
                                     "fechaPago, empleado_id FROM dbo.venta ORDER BY ID DESC";

            try
            {
                SqlDataReader resultadoSql = gestorDAO.ejecutarConsulta(obtenerVentaSql);

                if (resultadoSql.Read())
                {

                    venta.Id = resultadoSql.GetInt32(0);

                    if (resultadoSql.GetBoolean(1) )
                        venta.EsServicioDeEnsamblajeAceptado = true;
                    else
                        venta.EsServicioDeEnsamblajeAceptado = false;

                    venta.CostoTotal = resultadoSql.GetDouble(2);
                    venta.Fecha = resultadoSql.GetDateTime(3);
                    venta.Empleado.IdPersona = resultadoSql.GetInt32(4);
                    
                }

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar obtenete la venta", err);
            }

            return venta;
        }

        public List<Venta> obtenerListaDeVentas(string dni)
        {
            List<Venta> ventas = new List<Venta>();

            string obtenerVentaSql = "SELECT V.ID, V.FECHAPAGO, V.ESSERVICIODEENSAMBLAJETERMINADO FROM VENTA V " +
                                    "INNER JOIN empleado E " +
                                    "ON V.EMPLEADO_ID = E.ID " +
                                    "INNER JOIN PERSONA P " +
                                    "ON P.ID = E.ID " +
                                    "WHERE V.esServicioDeEnsamblajeAceptado = 1  AND V.ESSERVICIODEENSAMBLAJETERMINADO = 0 AND P.DNI = '" + dni + "' " +
                                    "ORDER BY V.FECHAPAGO ASC;";

            try
            {
                SqlDataReader resultadoSql = gestorDAO.ejecutarConsulta(obtenerVentaSql);

                while (resultadoSql.Read())
                {
                    Venta venta = new Venta();
                    venta.Id = resultadoSql.GetInt32(0);
                    venta.Fecha = resultadoSql.GetDateTime(1);
                    venta.EsServicioDeEnsamblajeTerminado = resultadoSql.GetBoolean(2);
                    ventas.Add(venta);

                }

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar obtenete la venta", err);
            }

            return ventas;
        }



        public Venta obtenerListaDePedidosDeUnaVenta(Venta venta)
        {
            string obtenerPedidosSql = "SELECT PE.CANTIDAD, C.NOMBRE, C.TIPO, C.MARCA, C.CATEGORIA FROM PEDIDOCOMPONENTE PE " +
                                    "INNER JOIN COMPONENTE C " +
                                    "ON PE.COMPONENTE_ID = C.ID " +
                                    "WHERE PE.VENTA_ID = " + venta.Id + ";";

            try
            {
                SqlDataReader resultadoSql = gestorDAO.ejecutarConsulta(obtenerPedidosSql);

                while (resultadoSql.Read())
                {
                    Componente componente = new Componente();
                    componente.NombreComponente = resultadoSql.GetString(1);
                    componente.Tipo = resultadoSql.GetString(2);
                    componente.Marca = resultadoSql.GetString(3);
                    componente.Categoria = resultadoSql.GetString(4);

                    PedidoComponente pedidoComponente = new PedidoComponente();
                    pedidoComponente.CantidadComponentes = resultadoSql.GetInt32(0);
                    pedidoComponente.Componente = componente;

                    venta.ListaPedidoComponentes.Add(pedidoComponente);
                }

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar obtenete la venta", err);
            }

            return venta;
        }

        public void actualizarVenta(int id_venta)
        {
            string actualizarVentaSQL;

            actualizarVentaSQL = "update venta set esServicioDeEnsamblajeTerminado = 1 where esServicioDeEnsamblajeAceptado = 1 and id = @id;";

            try
            {
                SqlCommand comando;

                comando = gestorDAO.obtenerComandoSQL(actualizarVentaSQL);

                comando.Parameters.AddWithValue("@id", id_venta);
                comando.ExecuteNonQuery();

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar actualizar la venta", err);
            }
        }

        //////// TARJETA DE CREDITO ////////////
        //comando.Parameters.AddWithValue("@numero", tarjetaDePago.NumeroDeTarjeta);
        //comando.Parameters.AddWithValue("@csc", tarjetaDePago.CodigoCSC);
        //comando.Parameters.AddWithValue("@cliente_Id_Fk", 1);
    }
}
