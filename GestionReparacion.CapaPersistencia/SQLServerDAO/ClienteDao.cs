﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using GestionReparacion.CapaDominio.Contratos;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.CapaPersistencia.SQLServerDAO
{
    public class ClienteDao : IClienteDao
    {
        private GestorDAO gestorDAO;

        public ClienteDao(IGestorDAO gestorDAO)
        {
            this.gestorDAO = (GestorDAO)gestorDAO;
        }

        public void guardarCliente(Cliente cliente)
        {
            // Si es que se hace en este caso se tiene que hacer inserción en dos tablas, 
            // se creará un procedimiento 
        }

        public void actualizarCliente(Cliente cliente)
        {
            // Si es que se hace en este caso se tiene que hacer inserción en dos tablas, 
            // se creará un procedimiento 

        }

        public Cliente buscarClientePorDni(string dni)
        {
            Cliente cliente = null;

            string consultaSql = "SELECT P.ID, P.DNI, P.NOMBRE, P.APELLIDO " +
                                "FROM CLIENTE C INNER JOIN PERSONA P " +
                                "ON C.ID = P.ID " +
                                "WHERE P.DNI = '" + dni + "';";

            try
            {
                SqlDataReader resultadoSql = gestorDAO.ejecutarConsulta(consultaSql);

                if (resultadoSql.Read())
                {
                    cliente = new Cliente();
                    cliente.IdPersona = resultadoSql.GetInt32(0);
                    cliente.Dni = resultadoSql.GetString(1);
                    cliente.Nombre = resultadoSql.GetString(2);
                    cliente.Apellido = resultadoSql.GetString(3);
                }
            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al obtener el cliente", err);
            }

            return cliente;
        }


    }
}
