﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using GestionReparacion.CapaDominio.Contratos;
using GestionReparacion.CapaDominio.Entidades;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Remoting;

namespace GestionReparacion.CapaPersistencia.SQLServerDAO
{
    public class ComponenteDao : IComponenteDao
    {
        private GestorDAO gestorDAO;

        public ComponenteDao(IGestorDAO gestorDAO)
        {
            this.gestorDAO = (GestorDAO)gestorDAO;
        }

        public Boolean actualizarComponte(Venta venta)
        {
            string actualizarComponenteSQL;

            actualizarComponenteSQL = "UPDATE componente SET stock = @stock " +
                                      "WHERE id = @componente_Id;";

            try
            {
                SqlCommand comando;

                comando = gestorDAO.obtenerComandoSQL(actualizarComponenteSQL);

                //////////////// UPDATE ////////// COMPONENTE ////////////
                
                foreach (var item in venta.ListaPedidoComponentes)
                {
                    comando = gestorDAO.obtenerComandoSQL(actualizarComponenteSQL);

                    comando.Parameters.AddWithValue("@componente_Id", item.Componente.IdComponente);
                    comando.Parameters.AddWithValue("@stock", item.Componente.Stock);

                    comando.ExecuteNonQuery();
                }

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar actualizar el componente", err);
            }

            return true;
        }

        public List<Componente> buscarComponentes(string categoria)
        {
            List<Componente> componentes = new List<Componente>();

            string consultaSql = "SELECT * FROM dbo.componente WHERE categoria = '" + categoria + "'";

            try
            {
                
                SqlDataReader resultadoSql = gestorDAO.ejecutarConsulta(consultaSql);

                while (resultadoSql.Read())
                {
                    componentes.Add(obtenerComponentes(resultadoSql));
                }

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al buscar los componentes", err);
            }

            return componentes;
        }

        private Componente obtenerComponentes(SqlDataReader resultadoSql)
        {
            Componente componente = new Componente();

            componente.IdComponente = resultadoSql.GetInt32(0);
            componente.NombreComponente = resultadoSql.GetString(1);
            componente.Precio = resultadoSql.GetDouble(2);
            componente.Tipo = resultadoSql.GetString(3);
            componente.Stock = resultadoSql.GetInt32(4);
            componente.Dificultad = resultadoSql.GetInt32(5);
            componente.Marca = resultadoSql.GetString(6);
            componente.Categoria = resultadoSql.GetString(7);

            return componente;
        }


    }
}