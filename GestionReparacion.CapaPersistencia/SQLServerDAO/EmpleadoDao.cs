﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using GestionReparacion.CapaDominio.Contratos;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.CapaPersistencia.SQLServerDAO
{
    public class EmpleadoDao : IEmpleadoDao
    {
        private GestorDAO gestorDAO;

        public EmpleadoDao(IGestorDAO gestorDAO)
        {
            this.gestorDAO = (GestorDAO)gestorDAO;
        }

        #region Antiguo

        public void guardarEmpleado(Empleado empleado)
        {
            // Si es que se hace en este caso se tiene que hacer inserción en dos tablas, 
            // se creará un procedimiento 
        }

        public void actualizarEmpleado(Empleado empleado)
        {
            // Aca solo se actualiza el estado, deberia ser todo

            string actualizarEmpleadoSQL;

            actualizarEmpleadoSQL = "UPDATE calidad.Empleado_Reparador SET " +
                                        "estadoDisponible = @estadoDisponible, " +
                                        "WHERE idPersona = @idPersona";
            try
            {
                SqlCommand comando;

                comando = gestorDAO.obtenerComandoSQL(actualizarEmpleadoSQL);

                //comando.Parameters.AddWithValue("@estadoDisponible", empleado.EstadoDisponible);
                comando.Parameters.AddWithValue("@idPersona", empleado.IdPersona);

                comando.ExecuteNonQuery();

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar actualizar el Empleado", err);
            }
        }

        #endregion Antiguo

        #region Nuevo

        public Empleado obtenerEmpleado()
        {
            Empleado empleado = new Empleado(); 

            string consultaSql = "SELECT TOP(1) E.id, P.dni, P.nombre, P.apellido, E.tareasPendientes " +
                     "FROM empleado E INNER JOIN persona P ON E.id = P.id ORDER BY tareasPendientes ASC";

            try
            {
                SqlDataReader resultadoSql = gestorDAO.ejecutarConsulta(consultaSql);

                if (resultadoSql.Read())
                {
                    Componente componente = new Componente();

                    empleado.IdPersona = resultadoSql.GetInt32(0);
                    empleado.Dni = resultadoSql.GetString(1);
                    empleado.Nombre = resultadoSql.GetString(2);
                    empleado.Apellido = resultadoSql.GetString(3);
                    empleado.CantidadDeEnsamblajesAsignados = resultadoSql.GetInt32(4);
                    
                }

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema obtener el empleado", err);
            }

            return empleado;
        }

        public Empleado loguearEmpleado(string dni)
        {
            Empleado empleado = null;

            string consultaSql = "SELECT P.ID, P.DNI, P.NOMBRE, P.APELLIDO, E.TAREASPENDIENTES, E.cantidadPenalidad " +
                                "FROM EMPLEADO E inner join PERSONA P ON E.ID = P.ID " +
                                "WHERE P.DNI = '" + dni + "';";

            try
            {
                SqlDataReader resultadoSql = gestorDAO.ejecutarConsulta(consultaSql);

                if (resultadoSql.Read())
                {
                    empleado = new Empleado();
                    empleado.IdPersona = resultadoSql.GetInt32(0);
                    empleado.Dni = resultadoSql.GetString(1);
                    empleado.Nombre = resultadoSql.GetString(2);
                    empleado.Apellido = resultadoSql.GetString(3);
                    empleado.CantidadDeEnsamblajesAsignados = resultadoSql.GetInt32(4);
                    empleado.CantidadDePenalidades = resultadoSql.GetInt32(5);
                }
            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al loguear el empleado", err);
            }

            return empleado;
        }
        public Boolean actualizarEmpleado(Venta venta)
        {
            string actualizarEmpleadoSQL;

            actualizarEmpleadoSQL = "UPDATE empleado SET tareasPendientes = @tareasPendientes " +
                                    "WHERE id = @empleado_id;";

            try
            {
                SqlCommand comando;

                comando = gestorDAO.obtenerComandoSQL(actualizarEmpleadoSQL);

                //////////////// UPDATES ////////// EMPLEADO ////////////
                comando.Parameters.AddWithValue("@tareasPendientes", venta.Empleado.CantidadDeEnsamblajesAsignados);
                comando.Parameters.AddWithValue("@empleado_id", venta.Empleado.IdPersona);

                comando.ExecuteNonQuery();

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar actualizar el empleado", err);
            }

            return true;
        }



        #endregion Nuevo

    }
}
