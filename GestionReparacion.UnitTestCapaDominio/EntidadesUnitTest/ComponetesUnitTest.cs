﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.UnitTestCapaDominio
{
    [TestClass]
    public class ComponetesUnitTest
    {
        [TestMethod]
        public void estaDisponibleTestMethod()
        {
            Componente componente = new Componente();
            componente.Stock = 5;

            Assert.IsTrue(componente.estaDisponible());
        }

    }
}
