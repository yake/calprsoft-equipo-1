﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionReparacion.CapaDominio.Entidades;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GestionReparacion.UnitTestCapaDominio.EntidadesUnitTest
{
    [TestClass]
    public class PedidoComponenteUnitTest
    {
        [TestMethod]
        public void calcularDescuentoPorMesFestivoTestMethod()
        {
            Componente componente = new Componente();
            componente.Precio = 160;
            
            PedidoComponente pedidoComponente = new PedidoComponente();
            pedidoComponente.CantidadComponentes = 8;
            pedidoComponente.Componente = componente;

            double resultadoEsperado = 64.0;
            double resultadoObtenido = pedidoComponente.calcularDescuentoPorMesFestivo();

            Assert.AreEqual(resultadoEsperado, resultadoObtenido);
        }

        [TestMethod]
        public void calcularDescuentoPorCantidadTestMethod()
        {   
            Componente componente = new Componente();
            componente.Precio = 25;

            PedidoComponente pedidoComponente = new PedidoComponente();
            pedidoComponente.CantidadComponentes = 15;
            pedidoComponente.Componente = componente;

            double resultadoEsperado = 37.5;
            double resultadoObtenido = pedidoComponente.calcularDescuentoPorCantidad();

            Assert.AreEqual(resultadoEsperado, resultadoObtenido);
        }

        [TestMethod]
        public void calcularDescuentoTotalTestMethod()
        {
            Componente componente = new Componente();
            componente.Precio = 80;

            PedidoComponente pedidoComponente = new PedidoComponente();
            pedidoComponente.CantidadComponentes = 17;
            pedidoComponente.Componente = componente;

            double resultadoObtenido = pedidoComponente.calcularDescuentoTotal();
            double resultadoEsperado = 204.0;

            Assert.AreEqual(resultadoEsperado, resultadoObtenido);
        }

        [TestMethod]
        public void calcularCostoTotalTestMethod()
        {
            Componente componente = new Componente();
            componente.Precio = 140;

            PedidoComponente pedidoComponente = new PedidoComponente();
            pedidoComponente.CantidadComponentes = 18;
            pedidoComponente.Componente = componente;

            double resultadoObtenido = pedidoComponente.calcularCostoTotal();
            double resultadoEsperado = 2142.0;

            Assert.AreEqual(resultadoEsperado, resultadoObtenido);
        }

        [TestMethod]
        public void calcularMontoTotalTestMethod()
        {
            Componente componente = new Componente();
            componente.Precio = 150;

            PedidoComponente pedidoComponente = new PedidoComponente();
            pedidoComponente.Componente = componente;
            pedidoComponente.CantidadComponentes = 4;

            double resultadoEsperado = 600.0;
            double resultadoObtenido = pedidoComponente.calcularMontoTotal();

            Assert.AreEqual(resultadoEsperado, resultadoObtenido);
        }

        [TestMethod]
        public void esValidoCantidadTestMethod()
        {
            Componente componente = new Componente();
            componente.Stock = 40;

            PedidoComponente pedidoComponente = new PedidoComponente();
            pedidoComponente.Componente = componente;
            pedidoComponente.CantidadComponentes = 2;

            Assert.IsTrue(pedidoComponente.esValidoCantidad());
        }

        [TestMethod]
        public void actualizarStockTestMethod()
        {
            Componente componente = new Componente();
            componente.Stock = 85;

            PedidoComponente pedidoComponente = new PedidoComponente();
            pedidoComponente.CantidadComponentes = 86;
            pedidoComponente.Componente = componente;

            Assert.IsFalse(pedidoComponente.actualizarStock());
        }
    }
}
