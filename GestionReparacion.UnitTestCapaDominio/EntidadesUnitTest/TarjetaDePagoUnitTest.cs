﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.UnitTestCapaDominio.EntidadesUnitTest
{
    [TestClass]
    public class TarjetaDePagoUnitTest
    {
        [TestMethod]
        public void esValidoNumeroDeTarjetaTestMethod()
        {
            TarjetaDePago tarjetaDePago = new TarjetaDePago();
            tarjetaDePago.NumeroDeTarjeta = "4557561238751786";

            Assert.IsTrue(tarjetaDePago.esValidoNumeroDeTarjeta());
        }

        [TestMethod]
        public void esValidoCodigoCSCTestMethod()
        {
            TarjetaDePago tarjetaDePago = new TarjetaDePago();
            tarjetaDePago.CodigoCSC = "8594";

            Assert.IsFalse(tarjetaDePago.esValidoCodigoCSC());
        }
    }
}
