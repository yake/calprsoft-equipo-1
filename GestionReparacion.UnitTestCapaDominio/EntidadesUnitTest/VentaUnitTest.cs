﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionReparacion.CapaDominio.Entidades;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GestionReparacion.UnitTestCapaDominio.EntidadesUnitTest
{
    [TestClass]
    public class VentaUnitTest
    {
        [TestMethod]
        public void calcularMontoPedidoTestMethod()
        {
            Componente componente1 = new Componente();
            componente1.Precio = 25;
            componente1.Stock = 85;

            Componente componente2 = new Componente();
            componente2.Precio = 45;
            componente2.Stock = 150;

            Componente componente3 = new Componente();
            componente3.Precio = 155;
            componente3.Stock = 240;

            PedidoComponente pedidoComponente1 = new PedidoComponente();
            pedidoComponente1.Componente = componente1;
            pedidoComponente1.CantidadComponentes = 15;

            PedidoComponente pedidoComponente2 = new PedidoComponente();
            pedidoComponente2.Componente = componente2;
            pedidoComponente2.CantidadComponentes = 8;

            PedidoComponente pedidoComponente3 = new PedidoComponente();
            pedidoComponente3.Componente = componente3;
            pedidoComponente3.CantidadComponentes = 17;

            List<PedidoComponente> listaPedidoComponentes = new List<PedidoComponente>();
            listaPedidoComponentes.Add(pedidoComponente1);
            listaPedidoComponentes.Add(pedidoComponente2);
            listaPedidoComponentes.Add(pedidoComponente3);

            Venta venta = new Venta();
            venta.ListaPedidoComponentes = listaPedidoComponentes;

            double resultadoEsperado = 2908.5;
            double resultadoObtenido = venta.calcularMontoPedido();

            Assert.AreEqual(resultadoEsperado, resultadoObtenido);
        }

        [TestMethod]
        public void calcularCostoDeEnsambladoTestMethod()
        {
            Componente componente1 = new Componente();
            componente1.Precio = 25;
            componente1.Dificultad = 10;
            componente1.Stock = 85;

            Componente componente2 = new Componente();
            componente2.Precio = 45;
            componente2.Dificultad = 40;
            componente2.Stock = 150;

            Componente componente3 = new Componente();
            componente3.Precio = 155;
            componente3.Dificultad = 35;
            componente3.Stock = 240;


            PedidoComponente pedidoComponente1 = new PedidoComponente();
            pedidoComponente1.Componente = componente1;
            pedidoComponente1.CantidadComponentes = 15;

            PedidoComponente pedidoComponente2 = new PedidoComponente();
            pedidoComponente2.Componente = componente2;
            pedidoComponente2.CantidadComponentes = 8;

            PedidoComponente pedidoComponente3 = new PedidoComponente();
            pedidoComponente3.Componente = componente3;
            pedidoComponente3.CantidadComponentes = 17;

            List<PedidoComponente> listaPedidoComponentes = new List<PedidoComponente>();
            listaPedidoComponentes.Add(pedidoComponente1);
            listaPedidoComponentes.Add(pedidoComponente2);
            listaPedidoComponentes.Add(pedidoComponente3);

            Venta venta = new Venta();
            venta.EsServicioDeEnsamblajeAceptado = true;
            venta.ListaPedidoComponentes = listaPedidoComponentes;

            double resultadoEsperado = 1103.75;
            double resultadoObtenido = venta.calcularCostoDeEnsamblado();

            Assert.AreEqual(resultadoEsperado, resultadoObtenido);
        }

        [TestMethod]
        public void calcularCostoTotalTestMethod()
        {
            Componente componente1 = new Componente();
            componente1.Precio = 25;
            componente1.Dificultad = 10;
            componente1.Stock = 85;

            Componente componente2 = new Componente();
            componente2.Precio = 45;
            componente2.Dificultad = 40;
            componente2.Stock = 150;

            Componente componente3 = new Componente();
            componente3.Precio = 155;
            componente3.Dificultad = 35;
            componente3.Stock = 240;


            PedidoComponente pedidoComponente1 = new PedidoComponente();
            pedidoComponente1.Componente = componente1;
            pedidoComponente1.CantidadComponentes = 15;

            PedidoComponente pedidoComponente2 = new PedidoComponente();
            pedidoComponente2.Componente = componente2;
            pedidoComponente2.CantidadComponentes = 8;

            PedidoComponente pedidoComponente3 = new PedidoComponente();
            pedidoComponente3.Componente = componente3;
            pedidoComponente3.CantidadComponentes = 17;

            List<PedidoComponente> listaPedidoComponentes = new List<PedidoComponente>();
            listaPedidoComponentes.Add(pedidoComponente1);
            listaPedidoComponentes.Add(pedidoComponente2);
            listaPedidoComponentes.Add(pedidoComponente3);

            Venta venta = new Venta();
            venta.EsServicioDeEnsamblajeAceptado = true;
            venta.ListaPedidoComponentes = listaPedidoComponentes;

            double resultadoEsperado = 4012.25;
            double resultadoObtenido = venta.calcularCostoTotal();

            Assert.AreEqual(resultadoEsperado, resultadoObtenido);
        }

        [TestMethod]
        public void tieneServicioDeEnsamblajeTestMethod()
        {
            Venta venta = new Venta();
            venta.EsServicioDeEnsamblajeAceptado = true;

            Assert.IsTrue(venta.tieneServicioDeEnsamblaje());
        }

        [TestMethod]
        public void estaElPlazoVencidoTestMethod()
        {
            Venta venta = new Venta();
            venta.Fecha = new DateTime(2020, 7, 1);

            Assert.IsTrue(venta.estaElPlazoVencido());
        }

        [TestMethod]
        public void aumentarPenalizacionEmpleadoTestMethod()
        {
            Empleado empleado = new Empleado();
            empleado.CantidadDePenalidades = 1;

            Venta venta = new Venta();
            venta.Fecha = new DateTime(2020, 7, 4);
            venta.Empleado = empleado;
            venta.aumentarPenalizacionEmpleado();

            int resultadoEsperado = 2;
            int resultadoObtenido = venta.Empleado.CantidadDePenalidades;

            Assert.AreEqual(resultadoEsperado, resultadoObtenido);
        }
    }
}
