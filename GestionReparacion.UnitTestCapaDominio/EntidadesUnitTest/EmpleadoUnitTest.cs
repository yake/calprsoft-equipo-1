﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.UnitTestCapaDominio.EntidadesUnitTest
{
    [TestClass]
    public class EmpleadoUnitTest
    {
        [TestMethod]
        public void estaPenalizadoTestMethod()
        {
            Empleado empleado = new Empleado();
            empleado.CantidadDePenalidades = 3;

            Assert.IsTrue(empleado.estaPenalizado());
        }
    }
}
